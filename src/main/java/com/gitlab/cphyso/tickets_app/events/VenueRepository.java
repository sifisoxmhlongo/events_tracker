package com.gitlab.cphyso.tickets_app.events;

import java.util.List;
import java.util.Optional;

public class VenueRepository {

    private final List<Venue> venueList = List.of(
        new Venue(45, "KingsMead", "West street", "Durban","South Africa"),
        new Venue(78, "Orange Building", "Pine street", "Accra","Ghana")
    );

    public Optional<Venue> findVenueById(int query){
        return venueList.stream().filter(venue -> venue.id() == query).findAny();
    }

}
