package com.gitlab.cphyso.tickets_app.events;

import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class OrganizerRepository {

    private static final List<Organizer> organizers = List.of(
        new Organizer(349,"FNB","Banking indaba"),
        new Organizer(500,"Agribiz","Farming indaba")
    );

    public List<Organizer> getOrganizerList() {
        return organizers;
    }
}
