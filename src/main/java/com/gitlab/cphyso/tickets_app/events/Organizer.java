package com.gitlab.cphyso.tickets_app.events;

public record Organizer(
    int id,
    String name,
    String description
) {
    
}
