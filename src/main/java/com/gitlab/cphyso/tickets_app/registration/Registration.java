package com.gitlab.cphyso.tickets_app.registration;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

@Document("registrations")
public record Registration(
    @Id String id,
    @NotNull(message = "product id is required")Integer productId,
    String ticketCode,
    @NotBlank(message = "name id is required")String attendeeName
    ) 
{
}