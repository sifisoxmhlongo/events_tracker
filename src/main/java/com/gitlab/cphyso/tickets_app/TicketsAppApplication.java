package com.gitlab.cphyso.tickets_app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TicketsAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(TicketsAppApplication.class, args);
	}
}
